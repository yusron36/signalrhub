﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalRHub.Hubs
{
    [HubName("MyHub")]
    public class MyHub : Hub
    {

        public void SendBroadcastMessage(string userid,string message)
        {
            Console.WriteLine(userid + " " + message);
            Clients.All.ReceiveBroadcastMessage(userid,message);
        }

    }
}
